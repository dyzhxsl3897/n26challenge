package com.n26.challenge.bl.controllers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.bl.services.TransactionService;

@RestController
@RequestMapping("/transactionservice/transaction")
public class TransactionController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TransactionService transactionServices;

	@RequestMapping(value = "/{transactionId}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Map<String, String>> putTransaction(@PathVariable("transactionId") final int transactionId,
			@RequestBody Transaction transaction) {
		logger.debug(transaction.toString());
		Map<String, String> status = new HashMap<String, String>();

		transaction.setId(transactionId);
		transactionServices.putTransaction(transaction);

		status.put("status", "ok");
		return new ResponseEntity<Map<String, String>>(status, HttpStatus.OK);
	}

	@RequestMapping(value = "/{transactionId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Transaction> getTransaction(@PathVariable("transactionId") final int transactionId) {
		logger.debug(String.valueOf(transactionId));
		Transaction transaction = transactionServices.getTransactionById(transactionId);

		return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
	}

}
