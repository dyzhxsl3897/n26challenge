package com.n26.challenge.bl.controllers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.n26.challenge.bl.services.TransactionSumService;

@RestController
@RequestMapping("/transactionservice/sum")
public class TransactionSumController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TransactionSumService transactionSumService;

	@RequestMapping(value = "/{transactionId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Map<String, Double>> getTransaction(@PathVariable("transactionId") final int transactionId) {
		logger.debug(String.valueOf(transactionId));
		double sum = transactionSumService.getSumByTransactionId(transactionId);

		Map<String, Double> result = new HashMap<String, Double>();
		result.put("sum", sum);

		return new ResponseEntity<Map<String, Double>>(result, HttpStatus.OK);
	}

}
