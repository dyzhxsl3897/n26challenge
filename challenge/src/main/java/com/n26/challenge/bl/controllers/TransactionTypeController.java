package com.n26.challenge.bl.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.n26.challenge.bl.services.TransactionTypeService;

@RestController
@RequestMapping("/transactionservice/types")
public class TransactionTypeController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TransactionTypeService transactionTypeService;

	@RequestMapping(value = "/{type}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Long>> getTransactionIdListByType(@PathVariable("type") final String type) {
		logger.debug(type);
		List<Long> transactionIdList = transactionTypeService.getTransactionIdListByType(type);

		return new ResponseEntity<List<Long>>(transactionIdList, HttpStatus.OK);
	}
}
