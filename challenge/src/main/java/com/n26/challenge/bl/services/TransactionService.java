package com.n26.challenge.bl.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.dao.TransactionReposity;

@Service
public class TransactionService {

	@Autowired
	private TransactionReposity dao;

	public void putTransaction(Transaction transaction) {
		dao.createTransaction(transaction);
	}

	public Transaction getTransactionById(long transactionId) {
		return dao.getTransactionById(transactionId);
	}

}
