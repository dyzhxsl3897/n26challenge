package com.n26.challenge.bl.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.challenge.dao.TransactionReposity;

@Service
public class TransactionSumService {

	@Autowired
	private TransactionReposity dao;

	public double getSumByTransactionId(long transactionId) {
		return dao.getSumByTransactionId(transactionId);
	}

}
