package com.n26.challenge.bl.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.challenge.dao.TransactionReposity;

@Service
public class TransactionTypeService {

	@Autowired
	private TransactionReposity dao;

	public List<Long> getTransactionIdListByType(String transactionType) {
		return dao.getTransactionIdListByType(transactionType);
	}

}
