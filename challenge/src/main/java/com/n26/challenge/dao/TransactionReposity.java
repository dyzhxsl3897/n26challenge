package com.n26.challenge.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.datasource.MockDataSource;

@Component
public class TransactionReposity {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MockDataSource mockDataSource = MockDataSource.getInstance();

	public void createTransaction(final Transaction transaction) {
		logger.debug("Insert a new transaction record");
		mockDataSource.insertTransaction(transaction);
	}

	public Transaction getTransactionById(final long transactionId) {
		logger.debug("Get a transaction record by a specified transaction id");
		return mockDataSource.selectTransactionById(transactionId);
	}

	public List<Long> getTransactionIdListByType(final String transactionType) {
		logger.debug("Get a list of all transaction ids that share the same type $type");
		return mockDataSource.selectTransactionIdListByType(transactionType);
	}

	public double getSumByTransactionId(final long transactionId) {
		logger.debug("Get sum of all transactions that are transitively linked by their parent_id to $transaction_id");
		return mockDataSource.selectSumByTransactionId(transactionId);
	}

}
