package com.n26.challenge.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.n26.challenge.bl.domain.Transaction;

public class MockDataSource {

	private List<Transaction> transactionList = new ArrayList<Transaction>();

	public void insertTransaction(final Transaction transaction) {
		synchronized (transactionList) {
			transactionList.add(transaction);
		}
	}

	public Transaction selectTransactionById(final long transactionId) {
		synchronized (transactionList) {
			for (Transaction transaction : transactionList) {
				if (transactionId == transaction.getId()) {
					return transaction;
				}
			}
		}
		return null;
	}

	public List<Long> selectTransactionIdListByType(final String transactionType) {
		List<Long> result = new ArrayList<Long>();
		synchronized (transactionList) {
			for (Transaction transaction : transactionList) {
				if (transactionType.equals(transaction.getType())) {
					result.add(transaction.getId());
				}
			}
		}
		return result;
	}

	public double selectSumByTransactionId(final long transactionId) {
		double sum = 0;
		Queue<Long> parentIdQueue = new LinkedList<Long>();
		synchronized (transactionList) {
			for (Transaction transaction : transactionList) {
				if (transactionId == transaction.getId()) {
					sum += transaction.getAmount();
					parentIdQueue.add(transaction.getId());
					break;
				}
			}
			while (!parentIdQueue.isEmpty()) {
				long currParentId = parentIdQueue.poll();
				for (Transaction transaction : transactionList) {
					if (currParentId == transaction.getParent_id()) {
						sum += transaction.getAmount();
						parentIdQueue.add(transaction.getId());
					}
				}
			}
		}
		return sum;
	}

	public void clearAllData() {
		synchronized (transactionList) {
			transactionList.clear();
		}
	}

	private MockDataSource() {
	}

	private static class SingletonHolder {
		private static final MockDataSource INSTANCE = new MockDataSource();
	}

	public static final MockDataSource getInstance() {
		return SingletonHolder.INSTANCE;
	}

}
