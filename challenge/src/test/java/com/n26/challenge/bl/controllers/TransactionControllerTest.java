package com.n26.challenge.bl.controllers;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.bl.services.TransactionService;
import com.n26.challenge.datasource.MockDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mockMvc;
	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Autowired
	private TransactionService transactionService;
	@Autowired
	private TransactionController transactionController;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

		MockDataSource.getInstance().clearAllData();
		Transaction transaction = new Transaction();
		transaction.setId(10);
		transaction.setAmount(5000);
		transaction.setType("cars");
		transactionService.putTransaction(transaction);
	}

	@Test
	public void testPutTransaction() throws Exception {
		Transaction transaction = new Transaction();
		transaction.setAmount(10000);
		transaction.setParent_id(10);
		transaction.setType("shopping");
		mockMvc.perform(put("/transactionservice/transaction/11").content(this.json(transaction)).contentType(contentType))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType)).andExpect(jsonPath("$.status").value("ok"));
		assertTrue(10000 == transactionService.getTransactionById(11).getAmount());
	}

	@Test
	public void testGetTransaction() throws Exception {
		mockMvc.perform(get("/transactionservice/transaction/10")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andExpect(content().encoding("UTF-8"))
				.andExpect(jsonPath("$").exists());
	}

	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}
