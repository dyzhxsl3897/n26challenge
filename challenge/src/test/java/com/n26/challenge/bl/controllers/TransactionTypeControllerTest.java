package com.n26.challenge.bl.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.bl.services.TransactionService;
import com.n26.challenge.datasource.MockDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionTypeControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mockMvc;

	@Autowired
	private TransactionService transactionService;
	@Autowired
	private TransactionTypeController transactionTypeController;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

		MockDataSource.getInstance().clearAllData();
		Transaction transaction = new Transaction();
		transaction.setId(10);
		transaction.setAmount(5000);
		transaction.setType("cars");
		transactionService.putTransaction(transaction);
		transaction = new Transaction();
		transaction.setId(11);
		transaction.setAmount(10000);
		transaction.setType("shopping");
		transaction.setParent_id(10);
		transactionService.putTransaction(transaction);
	}

	@Test
	public void testGetTransactionIdListByType() throws Exception {
		mockMvc.perform(get("/transactionservice/types/cars")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andExpect(content().encoding("UTF-8"))
				.andExpect(jsonPath("$", hasSize(1))).andExpect(jsonPath("$[0]").value(10));
	}
}
