package com.n26.challenge.bl.services;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.datasource.MockDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTest {

	@Autowired
	private TransactionService transactionService;

	@Before
	public void setUp() throws Exception {
		MockDataSource.getInstance().clearAllData();
		Transaction transaction = new Transaction();
		transaction.setId(10);
		transaction.setAmount(5000);
		transaction.setType("cars");
		transactionService.putTransaction(transaction);
	}

	@Test
	public void testPutTransaction() {
		Transaction transaction = new Transaction();
		transaction.setId(11);
		transaction.setAmount(10000);
		transaction.setType("shopping");
		transaction.setParent_id(10);
		transactionService.putTransaction(transaction);
		Transaction transactionSelected = transactionService.getTransactionById(11);
		assertTrue(null != transactionSelected && 10000 == transactionSelected.getAmount());
	}

	@Test
	public void testGetTransactionById() {
		assertTrue(null != transactionService.getTransactionById(10));
	}

}
