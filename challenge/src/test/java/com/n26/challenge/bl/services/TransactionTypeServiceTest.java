package com.n26.challenge.bl.services;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.n26.challenge.bl.domain.Transaction;
import com.n26.challenge.datasource.MockDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionTypeServiceTest {

	@Autowired
	private TransactionService transactionService;
	@Autowired
	private TransactionTypeService transactionTypeService;

	@Before
	public void setUp() throws Exception {
		MockDataSource.getInstance().clearAllData();
		Transaction transaction = new Transaction();
		transaction.setId(10);
		transaction.setAmount(5000);
		transaction.setType("cars");
		transactionService.putTransaction(transaction);
		transaction = new Transaction();
		transaction.setId(11);
		transaction.setAmount(10000);
		transaction.setType("shopping");
		transaction.setParent_id(10);
		transactionService.putTransaction(transaction);
	}

	@Test
	public void testGetTransactionIdListByType() {
		assertTrue(10 == transactionTypeService.getTransactionIdListByType("cars").get(0));
	}

}
